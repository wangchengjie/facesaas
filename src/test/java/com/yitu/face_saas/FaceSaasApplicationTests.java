package com.yitu.face_saas;

import com.yitu.face_saas.service.AddImage;
import com.yitu.face_saas.service.DeleteImage;
import net.bytebuddy.asm.Advice;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FaceSaasApplication.class)
@ContextConfiguration
public class FaceSaasApplicationTests {

    @Autowired
    private AddImage addImage;

    @Autowired
    private DeleteImage deleteImage;

    @Test
    public void contextLoads() {



    }


    @Test
    public void uploadImage(){
        addImage.uploadImage();
    }

    @Test
    public void deleteImage(){
        deleteImage.deleteImage();
    }

}
