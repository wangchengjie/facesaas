package com.yitu.face_saas.service.Impl;

import com.yitu.face_saas.service.AddImage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import sun.misc.BASE64Encoder;

import com.alibaba.fastjson.JSONObject;


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;


@Service
public class AddImageImpl implements AddImage {

    @Autowired
    private RestTemplate restTemplate;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${addImage.imagePath}")
    private String imagePath;

    @Value("${addImage.mode}")
    private Integer mode;

    @Value("${addImage.label}")
    private Integer label;

    @Value("${addImage.repo_name}")
    private String repo_name;

    @Value("${addImage.image_type}")
    private Integer image_type;

    @Value("${addImage.url}")
    private String url;

    @Override
    public void uploadImage() {

        // 遍历文件图片
        File file = new File(imagePath);
        File[] imageList = file.listFiles();

        // 格式化参数
        Map<String,Object> params = new HashMap<>();
        Map<String,Object> userInfo = new HashMap<>();
        Map<String,Object> options = new HashMap<>();
        options.put("image_type",1);
        params.put("mode",1);
        params.put("options",options);
        userInfo.put("repo_name",repo_name);
        userInfo.put("label",label);


        int upload = 0;
        int fail = 0;

        for (File image : imageList) {
            String imageName = image.getName();
            String userId = imageName.substring(imageName.lastIndexOf("_") + 1,imageName.lastIndexOf("."));
            userInfo.put("user_id",userId);
            userInfo.put("image_content",encodeBase64Image(image));
            params.put("user_info",userInfo);

            JSONObject rtn = restTemplate.postForEntity(url,JSONObject.toJSON(params),JSONObject.class).getBody();

            if ((Integer) rtn.get("rtn") == 0){
                upload++;
                logger.info("上传成功，图片名称：" + imageName);
            } else {
                fail++;
                logger.info("上传失败，图片名称：" + imageName + "失败原因：" + rtn.get("message"));
            }
        }

        logger.info("此次操作共" + upload + fail + "张图片，其中成功上传：" + upload + "张; 失败：" + fail + "张;");
    }



    public String encodeBase64Image(File image){

        byte[] data = null;

        try {
            InputStream in = new FileInputStream(image.getAbsoluteFile());
            data = new byte[in.available()];
            in.read(data);
            in.close();
        }catch (Exception e){

        }

        return new BASE64Encoder().encode(data).replaceAll("[\\s*\t\n\r]", "");
    }
}
