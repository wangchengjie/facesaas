package com.yitu.face_saas.service.Impl;

import com.alibaba.fastjson.JSONObject;
import com.yitu.face_saas.service.DeleteImage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

@Service
public class DeleteImageImpl implements DeleteImage {

    @Autowired
    private RestTemplate restTemplate;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${addImage.imagePath}")
    private String imagePath;

    @Value("${addImage.mode}")
    private Integer mode;

    @Value("${addImage.label}")
    private Integer label;

    @Value("${addImage.repo_name}")
    private String repo_name;

    @Value("${addImage.image_type}")
    private Integer image_type;

    @Value("${deleteImage.url}")
    private String url;

    @Override
    public void deleteImage() {

        Map<String, Object> params = new HashMap<>();
        params.put("repo_name", repo_name);
        params.put("label", 2);

        File file = new File(imagePath);
        File[] imageList = file.listFiles();

        String[] users = {"谷光烨_guangye.gu.jpg", "李志鹏_zhipeng.li1.jpg", "马一凡_yifan.ma.jpg", "杨胡田_hutian.yang.jpg", "苏晓鸣_xiaoming.su.jpg", "毛毅俊_yijun.mao.jpg", "张艳华_yanhua.zhang.jpg", "范文竹_wenzhu.fan.jpg", "欧阳希修_xixiu.ouyang.jpg", "Hazel Lee_hazel.lee.jpg", "程稳_wen.cheng.jpg", "周嘉文_jiawen.zhou.jpg", "姚征_zheng.yao2.jpg", "刘天树_tianshu.liu.jpg", "王嘉翼_jiayi.wang.jpg", "李孝东_xiaodong.li.jpg", "王烨_ye.wang.jpg", "刘威_wei.liu.jpg", "郑伯清_boqing.zheng.jpg", "杨贝贝_beibei.yang.jpg", "吴依桐_yitong.wu.jpg", "范勇_yong.fan.jpg", "吕利兰_lilan.lv.jpg", "Zhang Zining_zining.zhang.jpg", "洪韫妍_yunyan.hong.jpg", "朱升_sheng.zhu.jpg", "钟琳_lin.zhong.jpg", "刘书厂_shuchang.liu.jpg", "高瑞玲_ruiling.gao.jpg", "赵一南_yinan.zhao.jpg", "金嘉年_jianian.jin.jpg", "李娜_na.li.jpg", "高秉莉_bingli.gao.jpg", "任万钊_wanzhao.ren.jpg", "张弨_chao.zhang2.jpg", "张松_song.zhang.jpg", "王依雯_yiwen.wang.jpg", "李梓裕_ziyu.li.jpg", "王立群_liqun.wang.jpg", "姚梦_meng.yao.jpg", "周韬_tao.zhou.jpg", "戴海萍_haiping.dai.jpg", "Emily Ong_emily.ong.jpg", "陈剑文_jianwen.chen.jpg", "曹露_lu.cao.jpg", "严柯_ke.yan.jpg", "王佳祺_jiaqi.wang.jpg", "王淑贤_shuxian.wang.jpg", "罗轶斯_yisi.luo.jpg", "赵昕_xin.zhao.jpg", "刘宝罗_baoluo.liu.jpg", "唐玮鸿_weihong.tang.jpg", "邱凯翔_kaixiang.qiu.jpg", "戴峰_feng.dai.jpg", "吴志超_zhichao.wu.jpg", "吴胜琼_shengqiong.wu.jpg", "赵丹_dan.zhao.jpg", "邵育亮_yuliang.shao.jpg", "仇实_shi.qiu.jpg", "涂怀伟_huaiwei.tu.jpg", "陈小娟（BD）_xiaojuan.chen.jpg", "郝骥腾_jiteng.hao.jpg", "刘英志_yingzhi.liu.jpg", "李晓龙_xiaolong.li.jpg", "周大勇_dayong.zhou.jpg", "张元凯_yuankai.zhang.jpg", "范嘉骏_jiajun.fan.jpg", "李学远_xueyuan.li.jpg", "周龙_long.zhou.jpg", "吴永军_yongjun.wu.jpg", "颜泽鑫_zexin.yan.jpg", "刘兆伟_zhaowei.liu.jpg", "李成虎_chenghu.li.jpg", "康力_li.kang.jpg", "张玲_ling.zhang.jpg", "赵凯_kai.zhao.jpg", "季燮元_xieyuan.ji.jpg", "张小平_xiaoping.zhang.jpg", "祁利国_liguo.qi.jpg", "韦明书_mingshu.wei.jpg", "方淇_qi.fang.jpg", "刘雨晴_yuqing.liu.jpg", "高文博_wenbo.gao.jpg", "李金杰_jinjie.li.jpg", "邹一奇_yiqi.zou.jpg", "张恒_heng.zhang.jpg", "赵春昊_chunhao.zhao.jpg", "万一木_yimu.wan.jpg", "辛亮_liang.xin.jpg", "邵国鑫_guoxin.shao.jpg", "杨睿_rui.yang.jpg", "黄一恒_yiheng.huang.jpg", "陈海青_haiqing.chen.jpg", "刘洁妮_jieni.liu.jpg", "郝沛文_peiwen.hao.jpg", "王立鹏_lipeng.wang.jpg", "张凯伦_kailun.zhang.jpg", "应旭栋_xudong.ying.jpg", "Richard Yan_richard.yan.jpg", "朱星恒_xingheng.zhu.jpg", "张源耕_yuangeng.zhang.jpg", "郭超世_chaoshi.guo.jpg", "陈程_cheng.chen.jpg", "何艇_ting.he.jpg", "徐珺_jun.xu.jpg", "黄家凯_jiakai.huang.jpg", "王琛颖_chenying.wang.jpg", "刘伟_wei.liu3.jpg", "孟珠蓓_zhubei.meng.jpg", "江滢_ying.jiang.jpg", "支尚升_shangsheng.zhi.jpg", "李昊泽_haoze.li.jpg", "郝忠义_zhongyi.hao.jpg", "杜伦_lun.du.jpg", "黄皓_hao.huang.jpg", "刘雁文_yanwen.liu.jpg", "李晖_hui.li.jpg", "李崔卿_cuiqing.li.jpg", "陈昊_hao.ch.jpg", "黎青山_qingshan.li.jpg", "石光磊_guanglei.shi.jpg", "何倩_qian.he.jpg", "邹王忠_wangzhong.zou.jpg", "李宗衡_zongheng.li.jpg", "周誉昇_yusheng.zhou.jpg", "李梦钰_mengyu.li.jpg", "付荣荣_rongrong.fu.jpg", "庄文君_wenjun.zhuang.jpg", "金晓媛_xiaoyuan.jin.jpg", "李可达_keda.li.jpg", "王颖秋_yingqiu.wang.jpg", "双星级_xingji.shuang.jpg", "蒋松伦_songlun.jiang.jpg", "陈宏_hong.chen.jpg", "张凯_kai.zhang.jpg", "黄兆丰_zhaofeng.huang.jpg", "万金豪_jinhao.wan.jpg", "黄圣斯_shengsi.huang.jpg", "李萌_meng.li.jpg", "魏周蕊_zhourui.wei.jpg", "叶欢_huan.ye.jpg", "毛丹_dan.mao.jpg", "罗干_gan.luo.jpg", "王豫_yu.wang1.jpg", "柳建雨_jianyu.liu.jpg", "李超_chao.li.jpg", "刘子煜_ziyu.liu.jpg", "朱思猛_simeng.zhu.jpg", "金楠_nan.jin.jpg", "唐琦_qi.tang.jpg", "彭婧_jing.peng.jpg", "乔丽华_lihua.qiao.jpg", "董志盛_jason.dong.jpg", "胡琛临_chenlin.hu.jpg", "丁蓓_bei.ding.jpg", "曾庆熹_qingxi.zeng.jpg", "刘波_bo.liu.jpg", "彭瀚_penghan.jpg", "骆研_yan.luo.jpg", "顾云飞_yunfei.gu.jpg", "薛莹莹_yingying.xue.jpg", "陈卢鑫_luxin.chen.jpg", "王楠_nan.wang.jpg", "周育榕_yurong.zhou.jpg", "李俊雅_junya.li.jpg", "李启伟_qiwei.li.jpg", "戴世杰_shijie.dai.jpg", "赵孟洋_mengyang.zhao.jpg", "陈磊_lei.chen.jpg", "田卫平_weiping.tian.jpg", "朱梦羽_mengyu.zhu.jpg", "还俊杰_junjie.huan.jpg", "何皓_hao.he.jpg", "王子健_Tony.wang.jpg", "高仁华_renhua.gao.jpg", "袁威_wei.yuan.jpg", "梅飞_fei.mei.jpg", "刘婧_jing.liu.jpg", "Eliezer Toh_eliezer.toh.jpg", "李紫燕_ziyan.li.jpg", "吴文汐_wenxi.wu.jpg", "朱翔_xiang.zhu.jpg", "曹珲_hui.cao.jpg", "罗霄_xiao.luo.jpg", "刘朋丹_pengdan.liu.jpg", "陈妍妍_yanyan.chen.jpg", "黄璘璞_linpu.huang.jpg", "牛力彬_libin.niu.jpg", "吴博_bo.wu.jpg", "徐斌_sunny.xu.jpg", "王晓辉_xiaohui.wang.jpg", "李荣颉_rongjie.li.jpg", "王丽喆_lizhe.wang.jpg", "金紫晨_zichen.jin.jpg", "尹其畅_qichang.yin.jpg", "丁仁欢_renhuan.ding.jpg", "宋文旭_wenxu.song.jpg", "刘张鹏_liuzhang.peng.jpg", "张靖阳_jingyang.zhang.jpg", "杨红涛_hongtao.yang.jpg", "孙源_yuan.sun.jpg", "郭宋立_songli.guo.jpg", "夏冬_dong.xia.jpg", "张邵恒_shaoheng.zhang.jpg", "胡强_qiang.hu.jpg", "张子名_ziming.zhang.jpg", "陶韬_tao.tao.jpg", "闵熙栋_xidong.min.jpg", "王东明_dongming.wang.jpg", "林润骑_runqi.lin.jpg", "王杞_qi.wang2.jpg", "张天豪_tianhao.zhang.jpg", "王勇_yong.wang.jpg", "陆涛_tao.lu.jpg", "郭凯_kai.guo.jpg", "吴迪_di.wu.jpg", "方紫琳_zilin.fang.jpg", "张亦弛_yichi.zhang.jpg", "吴戈_ge.wu.jpg", "曹一迪_yidi.cao.jpg", "温悦_yue.wen.jpg", "袁海_hai.yuan.jpg", "梁平_ping.liang.jpg", "李施旖_shiyi.li.jpg", "张轩_xuan.zhang.jpg", "金丽圆_liyuan.jin.jpg", "陈尊_zun.chen.jpg", "沈志伟_zhiwei.shen.jpg", "张志齐_zhiqi.zhang.jpg", "康亮_liang.kang.jpg", "郭迁弟_qiandi.guo.jpg", "侯奇_qi.hou.jpg", "吴岷_min.wu.jpg", "周芸_yun.zhou.jpg", "谢君_jun.xie.jpg", "吴福乐_fule.wu.jpg", "林伟杰_weijie.lin.jpg", "涂雅雯_yawen.tu.jpg", "商琳琳_linlin.shang.jpg", "吴飞_fei.wu.jpg", "吴双_shuang.wu.jpg", "肖寒君_hanjun.xiao.jpg", "刘嘉_jia.liu.jpg", "梁卫华_weihua.liang.jpg", "李纲_gang.li.jpg", "叶宇航_yuhang.ye.jpg", "陈萌辉_menghui.chen.jpg", "李红旺_hongwang.li.jpg", "苏蕾_suzy.su.jpg", "吴天琪_tianqi.wu.jpg", "窦玉梅_yumei.dou.jpg", "邵子奇_ziqi.shao.jpg", "张峻_jun.zhang.jpg", "刁云怡_yunyi.diao.jpg", "杨中一_zhongyi.yang.jpg", "张弓发_gongfa.zhang.jpg", "廉大超_dachao.lian.jpg", "缪奇_qi.miao.jpg", "伍振华_zhenhua.wu.jpg", "梁朝亮_chaoliang.liang.jpg", "向富霖_fulin.xiang.jpg", "邓星_xing.deng.jpg", "于得水_deshui.yu.jpg", "王喆_zhe.wang.jpg", "唐颖_ying.tang.jpg", "孙梦莎_mengsha.sun.jpg", "丛慧_hui.cong.jpg", "李孟磊_menglei.li.jpg", "郑然_ran.zheng.jpg", "阮晓雯_xiaowen.ruan.jpg", "张凯胜_kaisheng.zhang.jpg", "陆程_cheng.lu-10955.jpg", "王红宾_hongbin.wang.jpg", "蔡嘉楠_jianan.cai.jpg", "郭振忠_zhenzhong.guo.jpg", "崔鑫_xin.cui.jpg", "程梦路_menglu.cheng.jpg", "陈若熹_ruoxi.chen.jpg", "蔡奕昌_Yichang Cai.jpg", "Darren Oh_darren.oh.jpg"};

        for (int i = 0; i < imageList.length; i++) {
            String imageName = imageList[i].getName();
            String userId = imageName.substring(imageName.lastIndexOf("_"), imageName.lastIndexOf("."));

            params.put("user_id", userId);
            restTemplate.delete(url, params);        }
//        for (File image : imageList) {
//
//            String imageName = image.getName();
//            String userId = imageName.substring(imageName.lastIndexOf("_"), imageName.lastIndexOf("."));
//
//
//            params.put("user_id", userId);
//            restTemplate.delete(url, params);
//
////            logger.info("上传成功，图片名称：" + imageName);
//        }

    }
}
